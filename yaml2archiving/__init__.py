from functools import lru_cache
import sys
from typing import Union

if sys.version_info >= (3, 8):
    from typing import TypedDict
else:
    from typing_extensions import TypedDict

import tango  # type: ignore


ARCHIVING_PARAMS = [
    "archive_abs_change",
    "archive_rel_change",
    "archive_period",
    "polling_period",
    "archive_strategy",
]


AttributeConfig = TypedDict(
    "AttributeConfig",
    {
        "polling_period": int,
        "code_push_event": bool,
        "archive_period": Union[int, str],
        "archive_rel_change": Union[float, str],
        "archive_abs_change": Union[float, str],
        "archive_strategy": str,
        "ttl": str,
    },
    total=False,
)


def get_attribute_config(attr_proxy: tango.AttributeProxy) -> AttributeConfig:
    config = attr_proxy.get_config()
    arch_event = config.events.arch_event
    result = AttributeConfig()
    if arch_event.archive_abs_change != "Not specified":
        result["archive_abs_change"] = float(arch_event.archive_abs_change)
    if arch_event.archive_rel_change != "Not specified":
        result["archive_rel_change"] = float(arch_event.archive_rel_change)
    if arch_event.archive_period != "Not specified":
        result["archive_period"] = int(arch_event.archive_period)
    polling_period = attr_proxy.get_poll_period()
    if polling_period:
        result["polling_period"] = polling_period
    return result


@lru_cache(0)
def get_attr_db_config(attribute: str) -> AttributeConfig:
    proxy = tango.AttributeProxy(attribute)
    return get_attribute_config(proxy)


def format_tango_exc(exc):
    return exc.args[-1].desc.splitlines()[0]
