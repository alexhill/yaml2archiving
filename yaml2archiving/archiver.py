from typing import Dict, Tuple
import logging
import time

import tango  # type: ignore

from . import AttributeConfig, get_attribute_config, format_tango_exc


def parse_archiver_attribute(line: str) -> Tuple[str, AttributeConfig]:
    """
    Parse out parameters stored in the archiver AttributeList property.
    AFAIK there is only strategy; all other config stored in event settings
    of the device.
    """
    attr_params = AttributeConfig()
    try:
        attr, *params = line.split(";")
    except ValueError:
        attr = line
    else:
        for param in params:
            name, value = param.split("=")
            if name == "strategy":
                attr_params["archive_strategy"] = value
    return attr, attr_params


def update_or_remove(d1, d2: AttributeConfig):
    """Update a dict, but remove Nones and 0."""
    for key, value in d2.items():
        if not value:  # None or 0 means no setting!
            d1.pop(key, None)
        else:
            d1[key] = value


def get_current_attributes(
    archiver: str,
    delay: float = 0,
    get_device_proxy=tango.DeviceProxy,
    get_attribute_proxy=tango.AttributeProxy,
) -> Tuple[Dict[str, AttributeConfig], Dict[str, tango.DevFailed]]:
    """
    Return the attributes currently configured, and their configs.
    Also return any attributes that could not be checked (e.g. device not running)
    """

    try:
        # Note that this works even if the archiver is offline!
        archiver_proxy = get_device_proxy(archiver)
        archiver_attrs = archiver_proxy.get_property("AttributeList")
    except tango.DevFailed as e:
        logging.error(
            "Could not get properties from archiver %r: %s", archiver, e.args[-1].desc
        )
        raise RuntimeError("Unable to get archiver config")
    archived_attributes: Dict[str, AttributeConfig] = {}
    broken_attributes = {}
    for line in archiver_attrs["AttributeList"]:
        time.sleep(delay)  # Relax so as to not overwhelm the database, etc
        attr, params = parse_archiver_attribute(line)
        try:
            attr_proxy = get_attribute_proxy(attr)
            attr_proxy.ping()
            # Get the archiving event config of the attribute
            current_params = get_attribute_config(attr_proxy)
            update_or_remove(params, current_params)
        except tango.DevFailed as exc:
            logging.error(
                "Could not get info from configured attr %s: %s",
                attr,
                format_tango_exc(exc),
            )
            broken_attributes[attr.lower()] = exc

        # This is tedious but needed to convince mypy that this is a valid AttributeConfig
        config = AttributeConfig()
        if "polling_period" in params:
            config["polling_period"] = params["polling_period"]
        if "code_push_event" in params:
            config["code_push_event"] = params["code_push_event"]
        if "archive_period" in params:
            config["archive_period"] = params["archive_period"]
        if "archive_rel_change" in params:
            config["archive_rel_change"] = params["archive_rel_change"]
        if "archive_abs_change" in params:
            config["archive_abs_change"] = params["archive_abs_change"]
        if "archive_strategy" in params:
            config["archive_strategy"] = params["archive_strategy"]
        if "ttl" in params:
            config["ttl"] = params["ttl"]
        archived_attributes[attr] = config
    return archived_attributes, broken_attributes


if __name__ == "__main__":
    import json
    import sys

    import yaml  # type: ignore

    config_file = sys.argv[1]
    with open(config_file) as f:
        config = yaml.load(f, Loader=yaml.FullLoader)
    attributes, broken = get_current_attributes(config["archiver"])
    print(json.dumps(attributes, indent=4))
