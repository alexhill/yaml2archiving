"""
Simple helper script to generate a configuration file from an existing
archive setup.

Not very clever or well tested, so you will want to check the resulting
file and clean it up.
"""
from functools import lru_cache
import logging
import re

import tango


@lru_cache(None)
def get_device_proxy(device):
    return tango.DeviceProxy(device)


def main(archiver):
    archiver_proxy = tango.DeviceProxy(archiver)
    attributes = archiver_proxy.read_attribute("AttributeList").value
    devices = {}
    for attribute in attributes:
        match = re.match(r"tango://[^:]+:\d+/(.*)/([^/]+)", attribute.lower())
        device = match.group(1)
        attr = match.group(2)
        try:
            device_proxy = get_device_proxy(device)
            info = device_proxy.info()
        except tango.DevFailed as e:
            logging.warning("Could not get info for %s (%s); skipping",
                            device, e.args[0].desc)
            continue
        att_conf = device_proxy.get_attribute_config(attr)
        poll_period = device_proxy.get_attribute_poll_period(attr)
        # Collect devices with same set of attributes and settings
        devices.setdefault((device, info.dev_class), {})[attr] = (
            att_conf.events.arch_event.archive_abs_change,
            att_conf.events.arch_event.archive_rel_change,
            att_conf.events.arch_event.archive_period,
            poll_period,
        )

    # "invert" the device mapping
    device_configs = {}
    for (device, clss), attrs in devices.items():
        attr_key = tuple(sorted(attrs.items()))  # must be hashable!
        device_configs.setdefault((clss, attr_key), set()).add(device)
    db = tango.Database()
    configurations = []
    # Now go through and build the configurations
    for (clss, attrs), devices in sorted(device_configs.items()):
        config = {
            "class": clss
        }
        class_devices = {d.lower() for d in db.get_device_exported_for_class(clss)}
        if set(class_devices) != devices:
            # These settings are not common for all devices of the class, so
            # we need to filter on device.
            # Note that there may be better ways to filter in individual cases
            # such as server name. Also, regex patterns. But for no let's not try
            # to be clever; the user can improve the config manually.
            config["filtering"] = {
                "device": sorted(devices)
            }
        for attr, (abs_change, rel_change, period, poll_period) in attrs:
            attr_config = {}
            if abs_change != "Not specified":
                attr_config["archive_abs_change"] = float(abs_change)
            if rel_change != "Not specified":
                attr_config["archive_rel_change"] = float(rel_change)
            if period != "Not specified":
                attr_config["archive_period"] = int(period)
            if poll_period:
                attr_config["polling_period"] = poll_period
            if attr_config:
                config.setdefault("attributes", {})[attr] = attr_config
        configurations.append(config)

    return {
        "db": f"{archiver_proxy.get_db_host()}:{archiver_proxy.get_db_port()}",
        "manager": "...",
        "archiver": archiver,
        "configuration": configurations,
    }


if __name__ == "__main__":
    from argparse import ArgumentParser

    import yaml

    parser = ArgumentParser()
    parser.add_argument("archiver")
    args = parser.parse_args()

    config = main(args.archiver)
    print(yaml.dump(config, sort_keys=False))
