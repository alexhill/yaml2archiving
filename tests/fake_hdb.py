from collections import defaultdict
import json
from random import random

from tango.server import Device, attribute, command, device_property


class HdbConfigurationManager(Device):

    """
    A device that looks like a HDB++ manager, but in fact does nothing
    except record commands being run.
    """

    def init_device(self):
        self._reset()
        self.added = []
        self.removed = []
        self.stopped = []

    def _reset(self):
        self.settings = defaultdict(None)

    @attribute(dtype=str)
    def SetArchiver(self):
        return self.settings["archiver"]

    @SetArchiver.write
    def write_SetArchiver(self, value):
        self.settings["archiver"] = value

    @attribute(dtype=str)
    def SetAttributeName(self):
        return self.settings["attribute_name"]

    @SetAttributeName.write
    def write_SetAttributeName(self, value):
        self.settings["attribute_name"] = value

    @attribute(dtype=float)
    def SetAbsoluteEvent(self):
        return self.settings["archive_abs_change"]

    @SetAbsoluteEvent.write
    def write_SetAbsoluteEvent(self, value):
        self.settings["archive_abs_change"] = value

    @attribute(dtype=float)
    def SetRelativeEvent(self):
        return self.settings["archive_rel_change"]

    @SetRelativeEvent.write
    def write_SetRelativeEvent(self, value):
        self.settings["archive_rel_change"] = value

    @attribute(dtype=int)
    def SetPeriodEvent(self):
        return self.settings["period_event"]

    @SetPeriodEvent.write
    def write_SetPeriodEvent(self, value):
        self.settings["period_event"] = value

    @attribute(dtype=int)
    def SetPollingPeriod(self):
        return self.settings["polling_period"]

    @SetPollingPeriod.write
    def write_SetPollingPeriod(self, value):
        self.settings["polling_period"] = value

    @attribute(dtype=bool)
    def SetCodePushedEvent(self):
        return self.settings["code_pushed_event"]

    @SetCodePushedEvent.write
    def write_SetCodePushedEvent(self, value):
        self.settings["code_pushed_event"] = value

    @attribute(dtype=str)
    def SetStrategy(self):
        return self.settings["strategy"]

    @SetStrategy.write
    def write_SetStrategy(self, value):
        self.settings["strategy"] = value

    @command
    def AttributeAdd(self):
        self.added.append(self.settings)
        self._reset()

    @command(dtype_out=str)
    def get_added(self):
        """ Test convenience command, not part of real HDB++ device """
        return json.dumps(self.added)

    @command(dtype_in=str)
    def AttributeRemove(self, attr):
        self.removed.append(attr)

    @command(dtype_out=[str])
    def get_removed(self):
        """ Test convenience command, not part of real HDB++ device """
        return self.removed

    @command(dtype_in=str)
    def AttributeStop(self, attr):
        self.stopped.append(attr)

    @command(dtype_out=[str])
    def get_stopped(self):
        """ Test convenience command, not part of real HDB++ device """
        return self.stopped


class HdbEventSubscriber(Device):

    attributelist = device_property(dtype=[str])

    @attribute(dtype=[str], max_dim_x=100)
    def AttributeList(self):
        return self.attributelist


class DummyDevice(Device):
    @attribute(dtype=float)
    def Apa(self):
        return random()

    @attribute(dtype=float)
    def bEpa(self):
        return random()

    @attribute(dtype=float)
    def cePa(self):
        return random()

    @attribute(dtype=float)
    def depA(self):
        return random()
