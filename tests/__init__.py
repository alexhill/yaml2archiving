from multiprocessing import Process, log_to_stderr
from time import sleep

import pytest
import tango  # type: ignore


def make_server_fixture(server, devices, green_mode=tango.GreenMode.Synchronous):

    """
    Build a test fixture that configures and starts a tango server.
    This assumes an existing Tango datbase.
    """

    def fixture():
        db = tango.Database()
        devinfos = []
        devclasses = set()
        for device, config in devices.items():
            devinfo = tango.DbDevInfo()
            devinfo.name = device
            devinfo._class = config["class"].__name__
            devinfo.server = server
            devinfos.append(devinfo)
            devclasses.add(config["class"])
        db.add_server(devinfo.server, devinfos, with_dserver=True)
        for device, config in devices.items():
            props = config.get("properties")
            if props:
                db.put_device_property(device, props)
            attr_props = config.get("attribute_properties")
            if attr_props:
                db.put_device_attribute_property(device, attr_props)

        log_to_stderr()
        proc = Process(
            target=tango.server.run,
            kwargs={
                "classes": tuple(devclasses),
                "args": server.split("/"),
                "green_mode": green_mode,
            },
            daemon=False,
        )

        proc.start()
        proxies = [tango.DeviceProxy(device) for device in devices]
        for proxy in proxies:
            while True:
                try:
                    proxy.ping()
                    break
                except tango.DevFailed:
                    sleep(0.1)

        yield proxies

        proc.terminate()
        db.delete_server(devinfo.server)

    return pytest.fixture(fixture, scope="function")
