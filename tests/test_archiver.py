from unittest.mock import Mock

import pytest
import tango

from yaml2archiving.archiver import get_current_attributes


def test_get_current_attributes():
    mock_device_proxy = Mock(spec=tango.DeviceProxy)
    attribute = "tango://some.tango.db:10000/test/dev/1/bananas"
    props = {
        "AttributeList": [attribute]
    }
    mock_device_proxy.get_property.return_value = props
    mock_attribute_proxy = Mock(spec=tango.AttributeProxy)
    mock_attribute_config = Mock(spec=tango.AttributeInfoEx)
    mock_event_info = Mock(spec=tango.AttributeEventInfo)
    mock_event_info.archive_rel_change = 3
    mock_event_info.archive_abs_change = 0
    mock_event_info.archive_period = 9182
    mock_attribute_config.events.arch_event = mock_event_info
    mock_attribute_proxy.get_poll_period.return_value = 2000

    mock_attribute_proxy.get_config.return_value = mock_attribute_config

    archived, broken = get_current_attributes(
        "my/great/archiver",
        get_device_proxy=lambda dev: mock_device_proxy,
        get_attribute_proxy=lambda dev: mock_attribute_proxy,
    )

    assert len(archived) == 1
    assert archived[attribute] == {
        "archive_rel_change": 3,
        # archive_abs_change not included since it's 0
        "archive_period": 9182,
        "polling_period": 2000
    }
    assert not broken


def test_get_current_attributes_broken_archiver():
    mock_device_proxy = Mock(spec=tango.DeviceProxy)
    mock_device_proxy.get_property.side_effect = tango.DevFailed(Mock(desc="hello"))

    with pytest.raises(RuntimeError):
        get_current_attributes(
            "my/great/archiver",
            get_device_proxy=lambda dev: mock_device_proxy,
        )


def test_get_current_attributes_broken_attribute():
    mock_device_proxy = Mock(spec=tango.DeviceProxy)
    attribute = "tango://some.tango.db:10000/test/dev/1/bananas"
    props = {
        "AttributeList": [attribute]
    }
    mock_device_proxy.get_property.return_value = props
    mock_attribute_proxy = Mock(spec=tango.AttributeProxy)
    mock_attribute_config = Mock(spec=tango.AttributeInfoEx)
    mock_event_info = Mock(spec=tango.AttributeEventInfo)
    mock_event_info.archive_rel_change = 3
    mock_event_info.archive_abs_change = 0
    mock_event_info.archive_period = 9182
    mock_attribute_config.events.arch_event = mock_event_info
    mock_attribute_proxy.get_poll_period.return_value = 2000

    mock_attribute_proxy.get_config.return_value = mock_attribute_config

    def get_attribute_proxy(attr):
        error_mock = Mock(desc="Some desc")
        raise tango.DevFailed(error_mock)

    archived, broken = get_current_attributes(
        "my/great/archiver",
        get_device_proxy=lambda dev: mock_device_proxy,
        get_attribute_proxy=get_attribute_proxy
    )

    assert len(archived) == 1
    assert archived[attribute] == {}
    assert len(broken) == 1
    assert attribute in broken
