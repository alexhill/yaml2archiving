"""
Hypothesis tests.
"""
from hypothesis import given, settings, HealthCheck
from hypothesis.strategies import lists
from hypothesis_jsonschema import from_schema
import pytest
import yaml

from yaml2archiving.config import (load_configuration)

from yaml2archiving.schema import CONFIG_SCHEMA, DEFAULTS_SCHEMA


@pytest.mark.skip("TODO: This test unreliable and can take a long time")
@settings(suppress_health_check=[HealthCheck.function_scoped_fixture, HealthCheck.too_slow])
@given(from_schema(CONFIG_SCHEMA), lists(from_schema(DEFAULTS_SCHEMA), max_size=3))
def test_hypothesis_load_configuration(tmp_path, config, defaults):
    """
    Generates config files from our schema, and checks that they pass.
    The tricky part is the validation after building the final config. It's hard
    to avoid e.g. generating configs that will contain non unique configs at that
    point, so this test will tend to break. I dont't think this is a problem in
    practice, but it means this test is not reliable. Still interesting to run
    manually from time to time, and maybe it can be improved.
    """
    if defaults:
        config["defaults"] = []
    else:
        config.pop("defaults", None)
    for i, default in enumerate(defaults):
        default_file = f"default{i}.yaml"
        with open(tmp_path / default_file, "w") as f:
            f.write(yaml.dump(default))
        config["defaults"].append("./" + default_file)
    with open(tmp_path / "config.yaml", "w") as f:
        f.write(yaml.dump(config))
    load_configuration(tmp_path / "config.yaml")
