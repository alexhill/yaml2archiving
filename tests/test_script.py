"""
Integration tests that require a tango DB.
These tests are much slower because they need to set up tango devices.
On the other hand they test the whole system (albeit with fake HDB++ devices)
"""

import json
import logging
import os
from pathlib import Path
import re
from tempfile import TemporaryDirectory
from textwrap import dedent
from uuid import uuid4

import tango
import pytest

from yaml2archiving.script import main, configure
from yaml2archiving import get_attribute_config
from . import make_server_fixture
from .fake_hdb import HdbConfigurationManager, HdbEventSubscriber, DummyDevice


UUID = str(uuid4())
DB = tango.ApiUtil.get_env_var("TANGO_HOST")
MANAGER = f"yaml2archiving/manager/{UUID}"
ARCHIVER = f"yaml2archiving/archiver/{UUID}"
DUMMY = f"yaml2archiving/dummy/{UUID}-1"
DUMMY2 = f"yaml2archiving/dummy/{UUID}-2"


devices = make_server_fixture(
    f"HdbConfigurationManager/yaml2archiving-{UUID}",
    {
        MANAGER: {
            # Fake manager device that does nothing but records actions
            "class": HdbConfigurationManager,
        },
        ARCHIVER: {
            "class": HdbEventSubscriber,
            "properties": {
                "AttributeList": [
                    f"tango://{DB}/{DUMMY}/apa;strategy=ALWAYS",
                    f"tango://{DB}/{DUMMY}/cepa;strategy=ALWAYS",
                    f"tango://{DB}/{DUMMY}/depa;strategy=ALWAYS",
                ]
            },
        },
        DUMMY: {
            "class": DummyDevice,
            "properties": {
                "polled_attr": ["apa", "3000", "cepa", "3000", "depa", "3000"],
            },
            "attribute_properties": {
                "apa": {"archive_rel_change": 47},
                "bepa": {"archive_abs_change": 2},
            },
        },
        DUMMY2: {
            "class": DummyDevice,
            "properties": {
                "polled_attr": ["apa", "3000", "cepa", "3000", "depa", "3000"],
            },
            "attribute_properties": {
                "apa": {"archive_rel_change": 47},
                "bepa": {"archive_abs_change": 2},
            },
        },
    },
)


def assert_added(manager, attr, strategy="ALWAYS", **kwargs):

    """Helper to assert that an attribute were added properly."""

    added = json.loads(manager.get_added())
    try:
        conf = dict(next(a for a in added if a.get("attribute_name") == attr))
    except StopIteration:
        raise AssertionError(f"addition {attr} not found!")
    conf.pop("attribute_name")
    assert conf.pop("strategy") == strategy, "Unexpected strategy!"
    for key, value in kwargs.items():
        assert key in conf
        confvalue = conf.pop(key)
        assert value == confvalue
    for key, value in conf.items():
        assert value is not None, f"Unset setting {key}!"
        assert not value, f"Unexpected setting {key}: {value}!"


def test_main():
    with TemporaryDirectory() as tmp:

        p = Path(tmp)
        subdir1 = p / "subdir1"
        subdir1.mkdir()
        subdir2 = subdir1 / "subdir2"
        subdir2.mkdir()

        configfile = subdir2 / "archiver.yaml"
        with (configfile).open("w") as f:
            f.write(f"""
            db: {DB}
            manager: {MANAGER}
            archiver: {ARCHIVER}
            configuration:
              - class: DummyDevice
                filtering:
                  device:
                  - {DUMMY}
                attributes:
                  apa:
                    polling_period: 3000
                    archive_rel_change: 47
            """)
        main([str(configfile), "--show"])


@pytest.mark.tangodb
def test_configure_basic(devices, capsys):
    manager, archiver, dummy, _ = devices

    with TemporaryDirectory() as tmp:

        p = Path(tmp)
        subdir1 = p / "subdir1"
        subdir1.mkdir()
        subdir2 = subdir1 / "subdir2"
        subdir2.mkdir()

        configfile = subdir2 / "archiver.yaml"
        with (configfile).open("w") as f:
            f.write(dedent(f"""
            db: {DB}
            manager: {MANAGER}
            archiver: {ARCHIVER}
            configuration:
              - class: DummyDevice
                filtering:
                  device:
                  - {DUMMY}
                attributes:
                  apa:
                    polling_period: 3000
                    archive_rel_change: 47
                  bepa:
                    polling_period: 2000
                  cepa:
                    archive_rel_change: 1
                  depa:
                    polling_period: 4000
            """))
        configure(configfile, update=False, write=False, show=False)
        captured = capsys.readouterr()
        output = captured.out

    # Check output
    assert len(re.findall(".*/apa", output)) == 0
    assert len(re.findall(fr"ADD\s+tango://{DB}/{DUMMY}/bepa", output)) == 1
    assert len(re.findall(fr"CHANGE\s+tango://{DB}/{DUMMY}/cepa", output)) == 1

    # Don't change polling period for depa, since it would be slower
    assert len(re.findall("polling_period:.*.->", output)) == 1
    assert len(re.findall("polling_period: None -> 2000", output)) == 1  # bepa

    # Check that nothing was actually done
    assert manager.get_added() == "[]"
    assert manager.get_stopped() == []
    assert manager.get_removed() == []


@pytest.mark.tangodb
def test_configure_small_change(devices, capsys):
    manager, archiver, dummy, _ = devices

    with TemporaryDirectory() as tmp:

        p = Path(tmp)
        subdir1 = p / "subdir1"
        subdir1.mkdir()
        subdir2 = subdir1 / "subdir2"
        subdir2.mkdir()

        configfile = subdir2 / "archiver.yaml"
        with (configfile).open("w") as f:
            f.write(dedent(f"""
            db: {DB}
            manager: {MANAGER}
            archiver: {ARCHIVER}
            configuration:
              - class: DummyDevice
                filtering:
                  device:
                  - {DUMMY}
                attributes:
                  apa:
                    polling_period: 3000
                    archive_rel_change: 47
                  cepa:
                    archive_rel_change: 1
                    polling_period: 3000
                  depa:
                    polling_period: 3000
            """))
        configure(configfile, update=False, write=False, show=False)
        captured = capsys.readouterr()
        output = captured.out

    # Check output
    changes = re.findall(fr"^CHANGE\s.*", output, flags=re.MULTILINE)
    assert len(changes) == 1
    assert "/cepa" in changes[0]
    assert "archive_rel_change" in output

    additions = re.findall(fr"ADD", output)
    assert len(additions) == 0

    # Check that nothing was actually done
    assert manager.get_added() == "[]"
    assert manager.get_stopped() == []
    assert manager.get_removed() == []


@pytest.mark.tangodb
def test_configure_update(devices, capsys):
    manager, archiver, dummy, _ = devices

    with TemporaryDirectory() as tmp:

        p = Path(tmp)
        subdir1 = p / "subdir1"
        subdir1.mkdir()
        subdir2 = subdir1 / "subdir2"
        subdir2.mkdir()

        configfile = subdir2 / "archiver.yaml"
        with (configfile).open("w") as f:
            f.write(dedent(f"""
            db: {DB}
            manager: {MANAGER}
            archiver: {ARCHIVER}
            configuration:
              - class: DummyDevice
                filtering:
                  device:
                  - {DUMMY}
                attributes:
                  bepa:
                    polling_period: 2000
                  cepa:
                    archive_rel_change: 1
                  depa:
                    polling_period: 4000
            """))
        configure(configfile, update=True, write=False, show=False)
        captured = capsys.readouterr()
        output = captured.out

    # Check output
    assert len(re.findall(fr"SKIP REMOVE\s+tango://{DB}/{DUMMY}/apa", output)) == 1
    assert len(re.findall(fr"ADD\s+tango://{DB}/{DUMMY}/bepa", output)) == 1
    assert len(re.findall(fr"CHANGE\s+tango://{DB}/{DUMMY}/cepa", output)) == 1

    # Don't change polling period for depa, since it would be slower
    assert len(re.findall("polling_period:.*.->", output)) == 1
    assert len(re.findall("polling_period: None -> 2000", output)) == 1  # bepa

    # Check that nothing was actually done
    assert manager.get_added() == "[]"
    assert manager.get_stopped() == []
    assert manager.get_removed() == []


@pytest.mark.tangodb
def test_configure_ignores_case(devices, capsys):
    manager, archiver, dummy, _ = devices

    with TemporaryDirectory() as tmp:

        p = Path(tmp)
        subdir1 = p / "subdir1"
        subdir1.mkdir()
        subdir2 = subdir1 / "subdir2"
        subdir2.mkdir()

        configfile = subdir2 / "archiver.yaml"
        with (configfile).open("w") as f:
            f.write(dedent(f"""
            db: {DB}
            manager: {MANAGER}
            archiver: {ARCHIVER}
            configuration:
              - class: DummyDEVICE
                filtering:
                  device:
                  - {DUMMY.upper()}
                attributes:
                  APA:
                    polling_period: 3000
                    archive_rel_change: 47
                  bepA:
                    polling_period: 2000
                  cepa:
                    archive_rel_change: 1
                  DEpa:
                    polling_period: 4000
            """))

        configure(configfile, update=False, write=False, show=False)
        captured = capsys.readouterr()
        output = captured.out

    # Check output
    assert len(re.findall(".*/apa", output)) == 0
    assert len(re.findall(fr"ADD\s+tango://{DB}/{DUMMY}/bepa", output)) == 1
    assert len(re.findall(fr"CHANGE\s+tango://{DB}/{DUMMY}/cepa", output)) == 1

    # Don't change polling period for depa, since it would be slower
    assert len(re.findall("polling_period:.*.->", output)) == 1
    assert len(re.findall("polling_period: None -> 2000", output)) == 1  # bepa

    # Check that nothing was actually done
    assert manager.get_added() == "[]"
    assert manager.get_stopped() == []
    assert manager.get_removed() == []


@pytest.mark.tangodb
def test_configure_no_change(devices, capsys):
    manager, archiver, dummy, _ = devices

    with TemporaryDirectory() as tmp:

        p = Path(tmp)
        subdir1 = p / "subdir1"
        subdir1.mkdir()
        subdir2 = subdir1 / "subdir2"
        subdir2.mkdir()

        configfile = subdir2 / "archiver.yaml"
        with (configfile).open("w") as f:
            f.write(dedent(f"""
            db: {DB}
            manager: {MANAGER}
            archiver: {ARCHIVER}
            configuration:
              - class: DummyDevice
                filtering:
                  device:
                  - {DUMMY}
                attributes:
                  apa:
                    polling_period: 3000
                    archive_rel_change: 47
                  cepa:
                    polling_period: 3000
                  depa:
                    polling_period: 3000
            """))

        configure(configfile, update=False, write=False, show=False)
        captured = capsys.readouterr()
        output = captured.out

    # Check output
    assert len(re.findall("Nothing to do", output)) == 1

    # Check that nothing was actually done
    assert manager.get_added() == "[]"
    assert manager.get_stopped() == []
    assert manager.get_removed() == []


@pytest.mark.tangodb
def test_configure_bad_db(devices, caplog):
    manager, archiver, dummy, _ = devices
    old_db = os.environ.get("TANGO_HOST")
    bad_db = "this.db.definitely.does.not.exist.xyz:12345"
    with TemporaryDirectory() as tmp:

        p = Path(tmp)
        subdir1 = p / "subdir1"
        subdir1.mkdir()
        subdir2 = subdir1 / "subdir2"
        subdir2.mkdir()

        configfile = subdir2 / "archiver.yaml"
        with (configfile).open("w") as f:
            f.write(dedent(f"""
            db: {bad_db}
            manager: {MANAGER}
            archiver: {ARCHIVER}
            configuration:
              - class: DummyDevice
                filtering:
                  device:
                  - {DUMMY}
                attributes:
                  nopa:  # Nonexistent attribute
                    polling_period: 4000
            """))
        caplog.set_level(logging.ERROR)
        with pytest.raises(RuntimeError):
            configure(configfile, update=False, write=True, show=False)

    # Check that we log the error
    assert len(caplog.record_tuples) >= 1
    msgs = [tup[2] for tup in caplog.record_tuples]
    assert any(bad_db in msg for msg in msgs)

    if old_db:
        os.environ["TANGO_HOST"] = old_db  # prevent affecting other tests


@pytest.mark.tangodb
def test_configure_broken_attribute(devices, caplog):
    manager, archiver, dummy, _ = devices

    with TemporaryDirectory() as tmp:

        p = Path(tmp)
        subdir1 = p / "subdir1"
        subdir1.mkdir()
        subdir2 = subdir1 / "subdir2"
        subdir2.mkdir()

        configfile = subdir2 / "archiver.yaml"
        with (configfile).open("w") as f:
            f.write(dedent(f"""
            db: {DB}
            manager: {MANAGER}
            archiver: {ARCHIVER}
            configuration:
              - class: DummyDevice
                filtering:
                  device:
                  - {DUMMY}
                attributes:
                  nopa:  # Nonexistent attribute
                    polling_period: 4000
            """))
        caplog.set_level(logging.ERROR)
        configure(configfile, update=False, write=True, show=False)

    # Just check that we log something relevant
    assert len(caplog.record_tuples) >= 1
    msgs = [tup[2] for tup in caplog.record_tuples]
    assert any("Attribute nopa is not supported by device" in msg for msg in msgs)


@pytest.mark.tangodb
def test_configure_write(devices):
    manager, archiver, dummy, _ = devices
    assert dummy.get_attribute_config("bepa").events.arch_event.archive_abs_change == "2"
    with TemporaryDirectory() as tmp:

        p = Path(tmp)
        subdir1 = p / "subdir1"
        subdir1.mkdir()
        subdir2 = subdir1 / "subdir2"
        subdir2.mkdir()

        configfile = subdir2 / "archiver.yaml"
        with (configfile).open("w") as f:
            f.write(dedent(f"""
            db: {DB}
            manager: {MANAGER}
            archiver: {ARCHIVER}
            configuration:
              - class: DummyDevice
                filtering:
                  device:
                  - {DUMMY}
                attributes:
                  apa:
                    polling_period: 3000
                    archive_abs_change: 1
                  bepa:
                    polling_period: 1000
                    archive_rel_change: 1
            """))
        configure(configfile, update=False, write=True, show=False)

    # check that the expected operations were done
    added = json.loads(manager.get_added())
    assert len(added) == 1
    assert_added(
        manager, f"tango://{DB}/{DUMMY}/bepa",
        archiver=ARCHIVER,
        polling_period=1000,
        archive_rel_change=1,
        archive_abs_change=0
    )
    bepa_cfg = dummy.get_attribute_config("bepa")
    # The absolute setting has been removed
    assert bepa_cfg.events.arch_event.archive_abs_change == "Not specified"

    assert dummy.get_attribute_poll_period("apa") == 3000
    apa_cfg = dummy.get_attribute_config("apa")
    assert apa_cfg.events.arch_event.archive_rel_change == "Not specified"
    assert apa_cfg.events.arch_event.archive_abs_change == "1"
    assert set(manager.get_removed()) == {
        f"tango://{DB}/{DUMMY}/cepa",
        f"tango://{DB}/{DUMMY}/depa",
    }


@pytest.mark.tangodb
def test_get_attribute_config(devices):
    _, _, dummy, _ = devices

    att_info = dummy.get_attribute_config("apa")
    att_info.events.arch_event.archive_abs_change = "1.23"
    att_info.events.arch_event.archive_rel_change = "7"
    att_info.events.arch_event.archive_period = "500"
    dummy.set_attribute_config(att_info)

    att_proxy = tango.AttributeProxy(f"{dummy.name()}/apa")
    config = get_attribute_config(att_proxy)

    assert isinstance(config["archive_abs_change"], float)
    assert config["archive_abs_change"] == 1.23
    assert isinstance(config["archive_rel_change"], float)
    assert config["archive_rel_change"] == 7.0
    assert isinstance(config["archive_period"], int)
    assert config["archive_period"] == 500
