import re
from typing import Any, NamedTuple
from unittest.mock import Mock

import pytest
import tango

from yaml2archiving.action import (get_safe_params, get_actions, perform_actions,
                                   show_actions, get_archiving_settings)


def fake_get_attribute_config(attr):
    return {
        "archive_abs_change": 14,
        "archive_rel_change": None,
        "archive_period": 10000,
        "polling_period": 1234,
    }


def test_get_safe_params():
    old = {
        "archive_rel_change": 6,
        "polling_period": 2000
    }
    new = {
        "archive_abs_change": 98,
        "polling_period": 1000
    }
    params = get_safe_params("blah", old, new)
    assert params == new


def test_get_safe_params_does_not_slow_polling():
    old = {
        "archive_rel_change": 6,
        "polling_period": 1000
    }
    new = {
        "archive_abs_change": 98,
        "polling_period": 2000
    }
    params = get_safe_params("blah", old, new)
    assert params == {
        "archive_abs_change": 98,
        "polling_period": 1000
    }


def test_get_safe_params_does_not_turn_off_polling():
    old = {
        "archive_rel_change": 6,
        "polling_period": 1000
    }
    new = {
        "archive_abs_change": 98,
    }
    params = get_safe_params("blah", old, new)
    assert params == {
        "archive_abs_change": 98,
        "polling_period": 1000
    }


def test_get_archiving_settings(mocker):

    class FakeTangoValue(NamedTuple):
        value: Any
        w_value: Any = None

    class FakeManagerProxy:

        attr_values = {
            "SetArchiver": FakeTangoValue("my/nice/archiver", "my/nice/archiver"),
            "SetAttributeName": FakeTangoValue("tango://my.cs:10000/sys/tg_test/1/ampli",
                                               "tango://my.cs:10000/sys/tg_test/1/ampli"),
            "SetCodePushedEvent": FakeTangoValue(False, True),
            "SetPeriodEvent": FakeTangoValue(0 ,10000),
            "SetPollingPeriod": FakeTangoValue(3000 ,3000),
            "SetAbsoluteEvent": FakeTangoValue(0.5, 0.4),
            "SetRelativeEvent": FakeTangoValue(4, 4)
        }

        def read_attribute(self, attr):
            return self.attr_values[attr]

    manager = FakeManagerProxy()
    settings = get_archiving_settings(manager)


def test_get_actions():

    old_attrs = {
        "sys/tg_test/1/ampli": {
            "archive_rel_change": 3,
            "archive_abs_change": 14,
            "archive_period": 10000,
            "polling_period": 1234
        },
        "sys/tg_test/1/double_scalar": {
            "archive_period": 10000,
            "polling_period": 1234
        }
    }
    new_attrs = {
        "sys/tg_test/1/ampli": {
            "archive_abs_change": 78,
            "polling_period": 1234,
        },
        "sys/tg_test/1/long_scalar": {
            "archive_abs_change": 16,
            "archive_period": 10000,
            "polling_period": 12345
        }
    }

    # Function under test
    actions = get_actions(old_attrs, new_attrs, {}, get_attr_db_config=fake_get_attribute_config)

    assert len(actions["added"]) == 1
    added_old, added_new = actions["added"]["sys/tg_test/1/long_scalar"]
    assert added_new["archive_abs_change"] == 16
    assert added_new["archive_period"] == 10000
    assert added_new["polling_period"] == 1234  # polling period not slowed

    assert len(actions["removed"]) == 1
    assert actions["removed"]["sys/tg_test/1/double_scalar"]

    assert len(actions["changed"]) == 1
    changed_old, changed_new = actions["changed"]["sys/tg_test/1/ampli"]
    changed_new["archive_rel_change"] = None
    changed_new["archive_abs_change"] = 78
    changed_new["archive_period"] = 10000
    changed_new["polling_period"] = changed_old["polling_period"] == 1234


def test_get_actions_with_broken_attr():

    old_attrs = {
        "sys/tg_test/1/ampli": {
            "archive_abs_change": 14,
            "archive_period": 10000,
            "polling_period": 1234
        },
        "sys/tg_test/1/double_scalar": {
            "archive_period": 10000,
            "polling_period": 1234
        }
    }
    new_attrs = {
        "sys/tg_test/1/ampli": {
            "archive_abs_change": 78,
            "polling_period": 1234,
        },
        "sys/tg_test/1/long_scalar": {
            "archive_abs_change": 16,
            "archive_period": 10000,
            "polling_period": 12345
        }
    }

    def fake_get_attribute_config_broken(attr):
        if attr == "sys/tg_test/1/long_scalar":
            raise tango.DevFailed(Mock(desc="Oops"))
        return fake_get_attribute_config(attr)

    # Function under test
    actions = get_actions(old_attrs, new_attrs, {},
                          get_attr_db_config=fake_get_attribute_config_broken)

    assert len(actions["added"]) == 0

    assert len(actions["removed"]) == 1
    assert actions["removed"]["sys/tg_test/1/double_scalar"]

    assert len(actions["changed"]) == 1
    changed_old, changed_new = actions["changed"]["sys/tg_test/1/ampli"]
    changed_new["archive_abs_change"] = 78
    changed_new["archive_period"] = 10000
    changed_new["polling_period"] = changed_old["polling_period"] == 1234

    assert len(actions["broken"]) == 1
    assert "sys/tg_test/1/long_scalar" in actions["broken"]


def test_get_actions_with_broken_existing_attr():

    """
    Pre broken means that the attribute is currently configured,
    but also could not be reached (e.g. device is not running).
    """

    old_attrs = {
        "sys/tg_test/1/ampli": {
            "archive_abs_change": 14,
            "archive_period": 10000,
            "polling_period": 1234
        },
        "sys/tg_test/1/double_scalar": {
            "archive_period": 10000,
            "polling_period": 1234
        }
    }
    new_attrs = {
        "sys/tg_test/1/ampli": {
            "archive_abs_change": 78,
            "polling_period": 1234,
        },
        "sys/tg_test/1/long_scalar": {
            "archive_abs_change": 16,
            "archive_period": 10000,
            "polling_period": 12345
        }
    }
    pre_broken_attrs = {
        "sys/tg_test/1/ampli": Exception
    }

    actions = get_actions(old_attrs, new_attrs, pre_broken_attrs,
                          get_attr_db_config=fake_get_attribute_config)

    assert len(actions["added"]) == 1
    added_old, added_new = actions["added"]["sys/tg_test/1/long_scalar"]
    assert added_new["archive_abs_change"] == 16
    assert added_new["archive_period"] == 10000
    assert added_new["polling_period"] == 1234  # polling period not slowed

    assert len(actions["removed"]) == 1
    assert actions["removed"]["sys/tg_test/1/double_scalar"]

    assert len(actions["changed"]) == 0

    assert len(actions["pre_broken"]) == 1
    assert "sys/tg_test/1/ampli" in actions["pre_broken"]


def test_perform_actions():

    manager = "sys/manager/1"
    archiver = "sys/archiver/1"
    added_attr = "sys/tg_test/1/ampli"
    removed_attr = "sys/tg_test/1/double_scalar"
    changed_attr = "sys/tg_test/1/long_scalar"

    actions = {
        "added": {
            added_attr: (
                {},
                {
                    "archive_rel_change": 5,
                    "archive_abs_change": 18,
                    "archive_period": 27,
                    "polling_period": 1000
                }
            )
        },
        "removed": {
            removed_attr: {}
        },
        "changed": {
            changed_attr: (
                {
                    "archive_rel_change": 5,
                    "archive_abs_change": 18,
                    "archive_period": 27,
                    "polling_period": 1000
                },
                {
                    "archive_rel_change": 50,
                    "archive_abs_change": None,
                    "archive_period": 1000,
                    "polling_period": 3000
                },
            )
        },
    }

    manager_proxy = Mock()

    def get_device_proxy(device):
        return manager_proxy

    attribute_proxy = Mock()
    attribute_config = Mock()
    attribute_proxy.get_config.return_value = attribute_config

    def get_attribute_proxy(device):
        return attribute_proxy

    # Function under test
    perform_actions(manager, archiver, actions,
                    get_device_proxy=get_device_proxy,
                    get_attribute_proxy=get_attribute_proxy)

    # Additions
    assert manager_proxy.SetArchiver == archiver
    assert manager_proxy.SetAttributeName == added_attr
    assert manager_proxy.SetRelativeEvent == 5
    assert manager_proxy.SetAbsoluteEvent == 18
    assert manager_proxy.SetPeriodEvent == 27
    assert manager_proxy.SetPollingPeriod == 1000
    manager_proxy.AttributeAdd.assert_called_once()

    # Removals
    manager_proxy.AttributeRemove.assert_called_once_with(removed_attr)

    # Changes
    assert attribute_config.events.arch_event.archive_rel_change == "50"
    assert attribute_config.events.arch_event.archive_abs_change == "0"
    assert attribute_config.events.arch_event.archive_period == "1000"
    attribute_proxy.poll.assert_called_once_with(3000)


def test_perform_actions_update():

    manager = "sys/manager/1"
    archiver = "sys/archiver/1"
    added_attr = "sys/tg_test/1/ampli"
    removed_attr = "sys/tg_test/1/double_scalar"
    changed_attr = "sys/tg_test/1/long_scalar"

    actions = {
        "added": {
            added_attr: (
                {},
                {
                    "archive_rel_change": 5,
                    "archive_abs_change": 18,
                    "archive_period": 27,
                    "polling_period": 1000
                }
            )
        },
        "removed": {
            removed_attr: {}
        },
        "changed": {
            changed_attr: (
                {
                    "archive_rel_change": 5,
                    "archive_abs_change": 18,
                    "archive_period": 27,
                    "polling_period": 1000
                },
                {
                    "archive_rel_change": 50,
                    "archive_abs_change": None,
                    "archive_period": 1000,
                    "polling_period": 3000
                },
            )
        },
    }

    manager_proxy = Mock()

    def get_device_proxy(device):
        return manager_proxy

    attribute_proxy = Mock()
    attribute_config = Mock()
    attribute_proxy.get_config.return_value = attribute_config

    def get_attribute_proxy(device):
        return attribute_proxy

    # Function under test
    perform_actions(manager, archiver, actions,
                    get_device_proxy=get_device_proxy,
                    get_attribute_proxy=get_attribute_proxy,
                    update=True)

    # Additions
    assert manager_proxy.SetArchiver == archiver
    assert manager_proxy.SetAttributeName == added_attr
    assert manager_proxy.SetRelativeEvent == 5
    assert manager_proxy.SetAbsoluteEvent == 18
    assert manager_proxy.SetPeriodEvent == 27
    assert manager_proxy.SetPollingPeriod == 1000
    manager_proxy.AttributeAdd.assert_called_once()

    # Removals
    # Update flag should prevent any attributes from being removed
    manager_proxy.AttributeRemove.assert_not_called()

    # Changes
    assert attribute_config.events.arch_event.archive_rel_change == "50"
    assert attribute_config.events.arch_event.archive_abs_change == "0"
    assert attribute_config.events.arch_event.archive_period == "1000"
    attribute_proxy.poll.assert_called_once_with(3000)
    attribute_proxy.set_config.assert_called_once_with(attribute_config)


def test_perform_actions_unset_parameters():

    manager = "sys/manager/1"
    archiver = "sys/archiver/1"
    added_attr = "sys/tg_test/1/ampli"

    actions = {
        "added": {
            added_attr: (
                {},
                {
                    "archive_rel_change": 0,
                    "archive_abs_change": 0,
                    "archive_period": 0,
                    "polling_period": 1000
                }
            )
        },
        "removed": {},
        "changed": {},
        "broken": {},
        "pre_broken": {},
    }

    manager_proxy = Mock()

    def get_device_proxy(device):
        return manager_proxy

    attribute_proxy = Mock()
    attribute_config = Mock()
    attribute_proxy.get_config.return_value = attribute_config

    def get_attribute_proxy(device):
        return attribute_proxy

    perform_actions(manager, archiver, actions,
                    get_device_proxy=get_device_proxy,
                    get_attribute_proxy=get_attribute_proxy)

    # Unset
    assert attribute_config.events.arch_event.archive_rel_change == "Not specified"
    assert attribute_config.events.arch_event.archive_abs_change == "Not specified"
    assert attribute_config.events.arch_event.archive_period == "Not specified"


def test_perform_actions_broken_manager():

    manager = "sys/manager/1"
    archiver = "sys/archiver/1"

    actions = {
        "added": {},
        "removed": {},
        "changed": {},
    }

    manager_proxy = Mock()
    manager_proxy.ping.side_effect = tango.DevFailed(Mock(desc="Darn it!"))

    def get_device_proxy(device):
        return manager_proxy

    with pytest.raises(RuntimeError):
        perform_actions(manager, archiver, actions,
                        get_device_proxy=get_device_proxy,
                        get_attribute_proxy=None)


def test_perform_actions_handles_broken_device(caplog):

    manager = "sys/manager/1"
    archiver = "sys/archiver/1"
    broken_attr = "sys/tg_test/1/long_scalar"
    ok_attr = "sys/tg_test/2/long_scalar"

    actions = {
        "added": {},
        "removed": {},
        "changed": {
            broken_attr: (
                {
                    "archive_abs_change": 18,
                },
                {
                    "archive_abs_change": None,
                },
            ),
            ok_attr: (
                {
                    "archive_rel_change": 5,
                },
                {
                    "archive_rel_change": 50,
                },
            )
        },
    }

    manager_proxy = Mock()

    def get_device_proxy(device):
        return manager_proxy

    broken_attribute_proxy = Mock()

    def broken_fake_get_config():
        raise tango.DevFailed(Mock(desc="Darn it!"))

    broken_attribute_proxy.get_config = broken_fake_get_config

    ok_attribute_proxy = Mock()
    ok_attribute_config = Mock()

    def ok_fake_get_config():
        return ok_attribute_config
    ok_attribute_proxy.get_config = ok_fake_get_config

    def get_attribute_proxy(device):
        if device in broken_attr:
            return broken_attribute_proxy
        return ok_attribute_proxy

    # Function under test
    perform_actions(manager, archiver, actions,
                    get_device_proxy=get_device_proxy,
                    get_attribute_proxy=get_attribute_proxy)

    # Check that we log the failure
    assert len(caplog.records) == 1
    for record in caplog.records:
        assert record.levelname == "ERROR"
        assert broken_attr in record.message

    # We still configure the non broken stuff
    assert ok_attribute_config.events.arch_event.archive_rel_change == "50"


actions = {
    "added": {
        "tango://my/nice/device/added": (
            {"archive_abs_change": 1},
            {"archive_abs_change": 2},
        ),
        "tango://my/nice/device/added_broken": (
            {"archive_abs_change": 0},
            {"archive_abs_change": 1},
        ),
        "tango://my/nice/device/added_unchanged": (
            {"archive_period": None},
            {"archive_period": None},
        ),
    },
    "removed": {
        "tango://my/nice/device/removed": {"archive_period": 10000},
        "tango://my/nice/device/removed_broken": {"archive_period": 20000},
    },
    "changed": {
        "tango://my/nice/device/changed": (
            {"archive_rel_change": 3},
            {"archive_rel_change": 4},
        ),
        "tango://my/nice/device/changed_broken": (
            {"archive_rel_change": 5},
            {"archive_rel_change": 6},
        ),
        "tango://my/nice/device/changed_unchanged": (
            {"archive_rel_change": 7},
            {"archive_rel_change": 7},
        )
    },
    "broken": {
        "tango://my/nice/device/added_broken": tango.DevFailed(Mock(desc="Ouch!")),
        "tango://my/nice/device/unreferenced": tango.DevFailed(Mock(desc="Ouch!")),
    },
    "pre_broken": {
        "tango://my/nice/device/changed_broken": tango.DevFailed(Mock(desc="Ouch!")),
        "tango://my/nice/device/removed_broken": tango.DevFailed(Mock(desc="Ooh!")),
    },
}


def test_show_actions(capsys):
    """
    Basic test, to check that the code runs.
    """
    output = "\n".join(show_actions(actions))
    print(output)
    assert re.search("^ADD tango://my/nice/device/added", output, flags=re.MULTILINE)
    assert re.search("^ADD tango://my/nice/device/added_unchanged", output, flags=re.MULTILINE)
    assert re.search("^REMOVE tango://my/nice/device/removed", output, flags=re.MULTILINE)
    # We don't care if removed attributes are broken since we don't need to access them
    assert re.search("^REMOVE tango://my/nice/device/removed_broken", output, flags=re.MULTILINE)
    assert re.search("^CHANGE tango://my/nice/device/changed", output, flags=re.MULTILINE)
    assert re.search("^CHANGE tango://my/nice/device/changed_unchanged", output, flags=re.MULTILINE)
    assert re.search("^SKIP CHANGE tango://my/nice/device/changed_broken", output, flags=re.MULTILINE)
    assert re.search("^SKIP ADD tango://my/nice/device/added_broken", output, flags=re.MULTILINE)


def test_show_actions_update(capsys):
    """
    Basic test, to check that the code runs.
    """
    output = "\n".join(show_actions(actions, update=True))
    print(output)
    assert re.search("^ADD tango://my/nice/device/added", output, flags=re.MULTILINE)
    assert re.search("^ADD tango://my/nice/device/added_unchanged", output, flags=re.MULTILINE)
    assert not re.search("^REMOVE tango://my/nice/device/removed", output, flags=re.MULTILINE)
    assert re.search("^SKIP REMOVE tango://my/nice/device/removed", output, flags=re.MULTILINE)
    # We don't care if removed attributes are broken since we don't need to access them
    assert not re.search("^REMOVE tango://my/nice/device/removed_broken", output, flags=re.MULTILINE)
    assert re.search("^SKIP REMOVE tango://my/nice/device/removed_broken", output, flags=re.MULTILINE)
    assert re.search("^CHANGE tango://my/nice/device/changed", output, flags=re.MULTILINE)
    assert re.search("^CHANGE tango://my/nice/device/changed_unchanged", output, flags=re.MULTILINE)
    assert re.search("^SKIP CHANGE tango://my/nice/device/changed_broken", output, flags=re.MULTILINE)
    assert re.search("^SKIP ADD tango://my/nice/device/added_broken", output, flags=re.MULTILINE)
